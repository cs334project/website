import cgi
from google.appengine.api import users
import webapp2

MAIN_PAGE_HTML = """\
<!DOCTYPE html>
<html>
	<title>DB Project</title>

	<body>
		<h1>The Time</h1>
	
		<section>
			<p>
				Please make me nice and pretty.
			</p>
			
			<form action="/sign" method="post">
  			    <div><textarea name="content" rows="3" cols="60"></textarea></div>
   				<div><input type="submit" value="Sign Home Page"></div>
   			</form>

		</section>
	</body>
</html>
"""

class MainPage(webapp2.RequestHandler):
    def get(self):
        self.response.write(MAIN_PAGE_HTML)

class Home(webapp2.RequestHandler):
    def post(self):
        self.response.write('<html><body>You wrote:<pre>')
        self.response.write(cgi.escape(self.request.get('content')))
        self.response.write('</pre></body></html>')

app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/sign', Home),
], debug=True)
