<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>WAIT-LESS</title>

<meta name="keywords" content="restaurant company, free website template, CSS, HTML, templatemo.com" />

<meta name="description" content="restaurant company - free website template, HTML CSS layout" />

<link href="templatemo_style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="reflection.js"></script>

</head>

<body>

<div id="templatemo_container">

  <div class="templatemo_topmenu">

    <ul>
      <li><a href="index.html">MAIN PAGE</a></li>
      <li><a href="about_us.html">ABOUT US</a></li>
      <li><a href="#" class="current">MENUS</a></li>
      <li><a href="location.html">LOCATIONS</a></li>
      <li><a href="contact.html">CONTACT</a></li>
	  <li><a href="login.html">LOGIN/SIGN UP</a></li>
    </ul>

  </div>

  <div id="templatemo_topsection">WAIT-LESS</div>

  <div id="templatemo_headersection"><img src="http://i.imgur.com/NvK9wiq.jpg" class="reflect rheight20 ropacity25" alt="templatemo.com" width="806" height="166"  /></div>

	<div id="templatemo_content_section">
		<a name="top">
		<div id="quick_menu">
			| <a href="#Starter"><strong>Starter</strong></a> |
			<a href="#Grilled"><strong>Grilled</strong></a> |
			<a href="#Meat"><strong>Meat</strong></a> |
			<a href="#Burgers"><strong>Burgers</strong></a> |
			<a href="#Sider"><strong>Sider</strong></a> |
			<a href="#Pizza"><strong>Pizza</strong></a> |
			<a href="#Fish"><strong>Fish</strong></a> |
			<a href="#Deserts"><strong>Deserts</strong></a> |
			<br/>
			| <a href="#SoftDrink"><strong>Soft Drink</strong></a> |
			<a href="#AlcoholDrink"><strong>Alcohol Drink</strong></a> |
		</div>

		<div br_clear>
			<br/>
		</div>

		<div id="menu_item">
			<a name="Starter">		
				<h1>Starter</h1>
			</a>
			<?php
				$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
				$result = $mysqli->query("SELECT * FROM Menu WHERE type_tag = 'Starter' order by 	price");
				$resArr = array();
				while ($row = mysqli_fetch_assoc($result)){
					$resArr[] = $row;
				}
				
				for ($x = 0; $x < count($resArr); $x++) {
					$result = json_encode($resArr[$x]);
					$result2 = json_decode($result, true);
					
					echo '<div id="description">';
					echo '<div id="food_pic"><img src=';
					echo $result2['Picture'];
					echo ' /></div>';
					echo '<h2>';
					echo $result2['item_name'];
					echo '</h2><p>';
					echo $result2['description'];
					echo '</br></br><strong>R';
					echo $result2['price'];				
					echo '</strong></p></div>';					
				}
			?>
		</div>

		<div id="br_clear">
			<br/>
			<a href="#top">Back to top</a>
		</div>

		<div id="menu_item">
			<a name="Grilled">		
				<h1>Grilled</h1>
			</a>
			<?php
				$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
				$result = $mysqli->query("SELECT * FROM Menu WHERE type_tag = 'Grilled' order by 	price");
				$resArr = array();
				while ($row = mysqli_fetch_assoc($result)){
					$resArr[] = $row;
				}
				
				for ($x = 0; $x < count($resArr); $x++) {
					$result = json_encode($resArr[$x]);
					$result2 = json_decode($result, true);
					
					echo '<div id="description">';
					echo '<div id="food_pic"><img src=';
					echo $result2['Picture'];
					echo ' /></div>';
					echo '<h2>';
					echo $result2['item_name'];
					echo '</h2><p>';
					echo $result2['description'];
					echo '</br></br><strong>R';
					echo $result2['price'];				
					echo '</strong></p></div>';					
				}
			?>
		</div>

		<div id="br_clear">
			<br/>
			<a href="#top">Back to top</a>
		</div>

		<div id="menu_item">
			<a name="Meat">		
				<h1>Meat</h1>
			</a>
			<?php
				$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
				$result = $mysqli->query("SELECT * FROM Menu WHERE type_tag = 'Meat' order by 	price");
				$resArr = array();
				while ($row = mysqli_fetch_assoc($result)){
					$resArr[] = $row;
				}
				
				for ($x = 0; $x < count($resArr); $x++) {
					$result = json_encode($resArr[$x]);
					$result2 = json_decode($result, true);
					
					echo '<div id="description">';
					echo '<div id="food_pic"><img src=';
					echo $result2['Picture'];
					echo ' /></div>';
					echo '<h2>';
					echo $result2['item_name'];
					echo '</h2><p>';
					echo $result2['description'];
					echo '</br></br><strong>R';
					echo $result2['price'];				
					echo '</strong></p></div>';					
				}
			?>
		</div>

		<div id="br_clear">
			<br/>
			<a href="#top">Back to top</a>
		</div>

		<div id="menu_item">
			<a name="Burgers">			
				<h1>Burgers</h1>
			</a>
			<?php
				$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
				$result = $mysqli->query("SELECT * FROM Menu WHERE type_tag = 'Burger' order by price");	
				$resArr = array();
				while ($row = mysqli_fetch_assoc($result)){
					$resArr[] = $row;
				}
				
				for ($x = 0; $x < count($resArr); $x++) {
					$result = json_encode($resArr[$x]);
					$result2 = json_decode($result, true);
					
					echo '<div id="description">';
					echo '<div id="food_pic"><img src=';
					echo $result2['Picture'];
					echo ' /></div>';
					echo '<h2>';
					echo $result2['item_name'];
					echo '</h2><p>';
					echo $result2['description'];
					echo '</br></br><strong>R';
					echo $result2['price'];				
					echo '</strong></p></div>';					
				}
			?>
		</div>

		<div id="br_clear">
			<br/>
			<a href="#top">Back to top</a>
		</div>

		<div id="menu_item">
			<a name="Sider">		
				<h1>Sider</h1>
			</a>
			<?php
				$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
				$result = $mysqli->query("SELECT * FROM Menu WHERE type_tag = 'Sider' order by 	price");
				$resArr = array();
				while ($row = mysqli_fetch_assoc($result)){
					$resArr[] = $row;
				}
				
				for ($x = 0; $x < count($resArr); $x++) {
					$result = json_encode($resArr[$x]);
					$result2 = json_decode($result, true);
					
					echo '<div id="description">';
					echo '<div id="food_pic"><img src=';
					echo $result2['Picture'];
					echo ' /></div>';
					echo '<h2>';
					echo $result2['item_name'];
					echo '</h2><p>';
					echo $result2['description'];
					echo '</br></br><strong>R';
					echo $result2['price'];				
					echo '</strong></p></div>';					
				}
			?>
		</div>

		<div id="br_clear">
			<br/>
			<a href="#top">Back to top</a>
		</div>

		<div id="menu_item">
			<a name="Pizza">		
				<h1>Pizza</h1>
			</a>
			<?php
				$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
				$result = $mysqli->query("SELECT * FROM Menu WHERE type_tag = 'Pizza' order by 	price");
				$resArr = array();
				while ($row = mysqli_fetch_assoc($result)){
					$resArr[] = $row;
				}
				
				for ($x = 0; $x < count($resArr); $x++) {
					$result = json_encode($resArr[$x]);
					$result2 = json_decode($result, true);
					
					echo '<div id="description">';
					echo '<div id="food_pic"><img src=';
					echo $result2['Picture'];
					echo ' /></div>';
					echo '<h2>';
					echo $result2['item_name'];
					echo '</h2><p>';
					echo $result2['description'];
					echo '</br></br><strong>R';
					echo $result2['price'];				
					echo '</strong></p></div>';					
				}
			?>
		</div>

		<div id="br_clear">
			<br/>
			<a href="#top">Back to top</a>
		</div>

		<div id="menu_item">
			<a name="Fish">		
				<h1>Fish</h1>
			</a>
			<?php
				$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
				$result = $mysqli->query("SELECT * FROM Menu WHERE type_tag = 'Fish' order by 	price");
				$resArr = array();
				while ($row = mysqli_fetch_assoc($result)){
					$resArr[] = $row;
				}
				
				for ($x = 0; $x < count($resArr); $x++) {
					$result = json_encode($resArr[$x]);
					$result2 = json_decode($result, true);
					
					echo '<div id="description">';
					echo '<div id="food_pic"><img src=';
					echo $result2['Picture'];
					echo ' /></div>';
					echo '<h2>';
					echo $result2['item_name'];
					echo '</h2><p>';
					echo $result2['description'];
					echo '</br></br><strong>R';
					echo $result2['price'];				
					echo '</strong></p></div>';					
				}
			?>
		</div>

		<div id="br_clear">
			<br/>
			<a href="#top">Back to top</a>
		</div>

		<div id="menu_item">
			<a name="Deserts">
				<h1>Deserts</h1>
			</a>
		
			<?php
				$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
				$result = $mysqli->query("SELECT * FROM Menu WHERE type_tag = 'Desert' order by price");
				$resArr = array();
				while ($row = mysqli_fetch_assoc($result)){
					$resArr[] = $row;
				}
			
				for ($x = 0; $x < count($resArr); $x++) {
					$result = json_encode($resArr[$x]);
					$result2 = json_decode($result, true);
				
					echo '<div id="description">';
					echo '<div id="food_pic"><img src=';
					echo $result2['Picture'];
					echo ' /></div>';
					echo '<h2>';
					echo $result2['item_name'];
					echo '</h2><p>';
					echo $result2['description'];
					echo '</br></br><strong>R';
					echo $result2['price'];				
					echo '</strong></p></div>';					
				}
			?>
		</div>

		<div id="br_clear">
			<br/>
			<a href="#top">Back to top</a>
		</div>

		<div id="menu_item">
			<a name="SoftDrink">		
				<h1>Soft Drink</h1>
			</a>
			<?php
				$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
				$result = $mysqli->query("SELECT * FROM Menu WHERE type_tag = 'SoftDrink' order by 	price");
				$resArr = array();
				while ($row = mysqli_fetch_assoc($result)){
					$resArr[] = $row;
				}
				
				for ($x = 0; $x < count($resArr); $x++) {
					$result = json_encode($resArr[$x]);
					$result2 = json_decode($result, true);
					
					echo '<div id="description">';
					echo '<div id="food_pic"><img src=';
					echo $result2['Picture'];
					echo ' /></div>';
					echo '<h2>';
					echo $result2['item_name'];
					echo '</h2><p>';
					echo $result2['description'];
					echo '</br></br><strong>R';
					echo $result2['price'];				
					echo '</strong></p></div>';					
				}
			?>
		</div>

		<div id="br_clear">
			<br/>
			<a href="#top">Back to top</a>
		</div>

		<div id="menu_item">
			<a name="AlcoholDrink">		
				<h1>Alcohol Drink</h1>
			</a>
			<?php
				$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
				$result = $mysqli->query("SELECT * FROM Menu WHERE type_tag = 'AlcoholDrink' order by 	price");
				$resArr = array();
				while ($row = mysqli_fetch_assoc($result)){
					$resArr[] = $row;
				}
				
				for ($x = 0; $x < count($resArr); $x++) {
					$result = json_encode($resArr[$x]);
					$result2 = json_decode($result, true);
					
					echo '<div id="description">';
					echo '<div id="food_pic"><img src=';
					echo $result2['Picture'];
					echo ' /></div>';
					echo '<h2>';
					echo $result2['item_name'];
					echo '</h2><p>';
					echo $result2['description'];
					echo '</br></br><strong>R';
					echo $result2['price'];				
					echo '</strong></p></div>';					
				}
			?>
		</div>

		<div id="br_clear">
			<br/>
			<a href="#top">Back to top</a>
		</div>


	</div>

	<div id="templatemo_footer">Designed by <a href="about_us.html"><strong>Wait-less Dev Team</strong></a> <span class="templatemo_footer"></div>
</div>
</body>
</html>