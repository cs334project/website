<!DOCTYPE html>
<html>
<body>

<?php
function get_menu(){
	$mysqli = new mysqli("localhost","arjen","arjen123", "restaurant");
	$result = $mysqli->query("SELECT item_name, price, item_number, type_tag, description, course FROM Menu");
	$resArr = array();
	while($row = mysqli_fetch_assoc($result)){
		$resArr[] = $row;
	}
	echo json_encode($resArr);
}

function get_menu_tag($tag){
	$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
	$result = $mysqli->query("SELECT * FROM Menu WHERE type_tag = '$tag'");
	$resArr = array();
	while ($row = mysqli_fetch_assoc($result)){
		$resArr[] = $row;
	}
	echo json_encode($resArr);
}

function get_user($email) {
	$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
	$result = $mysqli->query("SELECT * FROM Users WHERE Email = '$email'");
	if (mysqli_num_rows($result) > 0) {
		return TRUE;
	} else {
		return FALSE;
	}
}

function get_waiter($email) {
	$mysqli = new mysqli("localhost", "arjen", "arjen123", "restaurant");
	$email_esc = $mysqli->real_escape_string($email);
	$result = $mysqli->query("SELECT * FROM Waiter WHERE Waiter_email = '$email_esc'");
	if (mysqli_num_rows($result) > 0) {
		return TRUE;
	} else {
		return FALSE;
	}
}

function create_user($name, $surname, $email, $phone, $iswaiter, $pass){
	if (get_user($email)) {
		echo "[0]";
	} else {
		$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
		$name_esc = $mysqli->real_escape_string($name);
		$surname_esc = $mysqli->real_escape_string($surname);
		$phone_esc = $mysqli->real_escape_string($phone);
		$email_esc = $mysqli->real_escape_string($email);
		$iswaiter_esc = $mysqli->real_escape_string($iswaiter);
		$pass_esc = $mysqli->real_escape_string($pass);
		$hash = hash("md5", $pass_esc);
		$result = $mysqli->query("INSERT INTO restaurant.Users (Name, Surname, Phone_number, Email, is_Waiter, Password) 
				VALUES ('$name_esc', '$surname_esc', '$phone_esc', '$email_esc', $iswaiter_esc, '$hash')");
		if (get_user($email)) {
			echo "[1]";
		}
		if ($iswaiter != 0){
			$result2 = $mysqli->query("INSERT INTO restaurant.Waiter (Waiter_email, Current_table, Shift_Number) VALUES ('$email_esc', 0, 0)" );
			if (get_waiter($email)) {
				echo "[2]";
			}
		}
	}
}

function check_table ($table_nr){
	$mysqli = new mysqli ("localhost", "arjen", "arjen123", "restaurant");
	$table_nr_esc = $mysqli->real_escape_string($table_nr);
	$result2 = $mysqli->query("SELECT Checkout FROM Bills WHERE Table_Number = '$table_nr_esc'");
	$arr = array();
	$row = mysqli_fetch_assoc($result2);
	$arr[] = $row;
	if (mysqli_num_rows($result2)>0) {
		echo json_encode($arr);
		return $arr;
	} else {
		echo "[0]";
		return 0;
	}
}

function get_bill ($bill_number) {
	$mysqli = new mysqli("localhost", "arjen", "arjen123", "restaurant");
	$bill_number_esc = $mysqli->real_escape_string($bill_number);
	$result = $mysqli->query("SELECT Menu.item_name , Bill_Items.Price AS Price,
	COUNT((SELECT Menu.item_name FROM Menu WHERE Menu.item_name = Menu.item_name AND Bill_Items.Items = Menu.item_number)) AS Quantity
	FROM Menu, Bill_Items
	WHERE Bill_Items.Bill_Number = '$bill_number_esc' AND Bill_Items.Items = Menu.item_number
	GROUP BY Menu.item_name;");
	$arr = array();
	while ($row = mysqli_fetch_assoc($result)) {
		$arr[] = $row;
	}
	echo json_encode($arr);
}

function create_bill ($user_id, $table_nr) {
	$mysqli = new mysqli("localhost","arjen","arjen123","restaurant");
	$user_id_esc = $mysqli->real_escape_string($user_id);
	$table_nr_esc = $mysqli->real_escape_string($table_nr);
	$result = $mysqli->query("INSERT INTO Bills (Waiter_email, User_email, Checkout, Table_Number) VALUES ((SELECT Waiter.Waiter_email FROM Waiter WHERE Waiter.Current_table = $table_nr_esc), '$user_id_esc', 0, $table_nr_esc );");
	 get_bill_number($table_nr);
}

function get_bill_number($table_number){
	$mysqli = new mysqli("localhost", "arjen", "arjen123", "restaurant");
	$table_nr_esc = $mysqli->real_escape_string($table_number);
	$result = $mysqli->query("SELECT Bills.Bill_Number FROM Bills WHERE Bills.Table_Number = '$table_nr_esc' AND Bills.Checkout != '1'");
	$arr = array();
	$row = mysqli_fetch_assoc($result);
	$arr[] = $row;
	if (mysqli_num_rows($result)>0) {
		echo json_encode($arr);
		return $arr;
	}else {
		echo "[0]";
		return "";
	}
}

function login($email, $pass){
	$mysqli = new mysqli("localhost", "arjen", "arjen123", "restaurant");
	$email_esc = $mysqli->real_escape_string($email);
	$pass_esc = $mysqli->real_escape_string($pass);
	$hash = hash("md5", $pass_esc);
	$result = $mysqli->query("SELECT Users.Email FROM Users WHERE Users.Password = '$hash' AND Users.Email = '$email_esc'");
	$arr = array();
	$row = mysqli_fetch_assoc($result);
	$arr[] = $row;
	if (mysqli_num_rows($result)>0) {
		echo "[1]";
	} else {
		echo "[0]";
	}
}

function checkout_bill($bill_number) {
	$mysqli = new mysqli("localhost", "arjen", "arjen123", "restaurant");
	$bill_nr_esc = $mysqli->real_escape_string($bill_number);
	$result = $mysqli->query("UPDATE Bills SET Bills.Checkout = 1 WHERE Bills.Bill_Number = '$bill_nr_esc';");
}

function order_item($bill_number, $item_number){
	$mysqli = new mysqli("localhost", "arjen", "arjen123", "restaurant");
	$bill_number_esc = $mysqli->real_escape_string($bill_number);
	$item_number_esc = $mysqli->real_escape_string($item_number);
	$result = $mysqli->query("INSERT INTO Bill_Items (Bill_number, Items, Price) VALUES ('$bill_number_esc', '$item_number_esc',
(SELECT Menu.Price FROM Menu WHERE Menu.Item_number = '$item_number'))");
	echo "[1]";
}

function get_bill_total($bill_number){
	$mysqli = new mysqli("localhost", "arjen", "arjen123", "restaurant");
	$bill_number_esc = $mysqli->real_escape_string($bill_number);
	$result = $mysqli->query("SELECT Bill_Items.Bill_Number, SUM((SELECT Menu.Price FROM Menu WHERE Menu.item_number = Bill_Items.Items))
AS Total_Price FROM Bill_Items WHERE Bill_Items.Bill_Number = '$bill_number_esc'");
	$arr = array();
	$row = mysqli_fetch_assoc($result);
	$arr[] = $row;
	echo json_encode($arr);
}

if ($_GET['func'] == 'get_menu'){
	$_GET['func']();
}else if ($_GET['func'] == 'get_menu_tag') {
	$_GET['func']($_GET['tag']);
}else if ($_GET['func'] == 'create_user') {
	$_GET['func']($_GET['name'], $_GET['surname'], $_GET['email'], $_GET['phone'], $_GET['iswaiter'], $_GET['password']);
}else if ($_GET['func'] == 'get_user') {
	$_GET['func']($_GET['email']);
}else if ($_GET['func'] == 'create_bill') {
	$_GET['func']($_GET['user_email'], $_GET['table_number']);
}else if ($_GET['func'] == 'check_table') {
	if ($herp = $_GET['func']($_GET['table_number'])!= 0){
		echo json_encode($herp);
	} else {
		echo "no bill on table";
	}
}else if ($_GET['func'] == 'get_bill_number') {
	$_GET['func']($_GET['table_number']);
}else if ($_GET['func'] == 'order_item') {
	$_GET['func']($_GET['bill_number'], $_GET['item_number']);
}else if ($_GET['func'] == 'checkout_bill') {
	$_GET['func']($_GET['bill_number']);
}else if ($_GET['func'] == 'login') {
	$_GET['func']($_GET['email'], $_GET['pass']);
}else if ($_GET['func'] == 'get_bill_total') {
	$_GET['func']($_GET['bill_number']);
}else if ($_GET['func'] == 'get_bill') {
	$_GET['func']($_GET['bill_number']);
}
?>


</body>
</html>
